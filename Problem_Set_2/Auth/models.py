from django.db import models

class app_details(models.Model):
    app_name=models.CharField(max_length=100)
    app_link=models.CharField(max_length=100)
    app_catagory=models.CharField(max_length=100)
    sub_catagory=models.CharField(max_length=100)
    app_points=models.IntegerField()

class user_submission(models.Model):
    username=models.CharField(max_length=100)
    appname=models.CharField(max_length=100)
    applink=models.CharField(max_length=100)
    points=models.IntegerField()
    screenshots=models.ImageField(upload_to='images/')

