from django.contrib import admin


from Auth.models import app_details, user_submission

admin.site.register(app_details)
admin.site.register(user_submission)

