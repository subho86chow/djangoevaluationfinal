# Generated by Django 4.0.4 on 2022-04-26 15:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Auth', '0002_alter_app_details_app_points'),
    ]

    operations = [
        migrations.CreateModel(
            name='user_submission',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=100)),
                ('appname', models.CharField(max_length=100)),
                ('applink', models.CharField(max_length=100)),
                ('points', models.IntegerField()),
                ('screenshot', models.ImageField(upload_to='images/')),
            ],
        ),
    ]
