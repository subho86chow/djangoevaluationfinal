from . import views
from django.contrib import admin
from django.urls import path , include

urlpatterns = [
    path('',views.home,name="home"),
    path('login',views.login_user,name="login"),
    path('signup',views.signup,name="signup"),
    path('admin_login',views.login_adminn,name="admin_login"),
    path('logout',views.logoutUser, name="logout"),
    path('user_home',views.user_home, name="user_home"),
    path('admin_home',views.admin_home, name="admin_home"),
    path('profile',views.user_profile, name="profile"),
    path('points',views.user_point, name="points"),
    path('add_apps',views.add_apps, name="add_apps"),
    path('create',views.app_details, name="app_details"),
    path('create',views.user_submission, name="user_submission"),
    path('edit/<str:app_name>',views.get_appdetail, name="edit"),
    path('delete/<str:app_name>',views.delete, name="delete"),
    path('display',views.display,name="display"),
    path('tasks',views.task_submit, name="tasks"),
    path('<int:id>',views.getapi),








    

]
 