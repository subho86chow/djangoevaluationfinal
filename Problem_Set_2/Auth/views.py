import email
from django.contrib import messages
from unicodedata import name
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from django.contrib.auth import logout, authenticate, login
# from Auth.forms import appform
from Auth.models import app_details, user_submission
import json



# Create your views here.
def home(request):
    if request.user.is_anonymous:
        return render(request,'Auth/login.html') 
    return render(request,"user_home.html")


def signup(request):
    if request.method == "POST":
        username = request.POST.get('username')
        fname = request.POST['fname']
        lname = request.POST['lname']
        email = request.POST.get('email')
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')
        if User.objects.filter(username = username).first():
            return render(request,"Auth/signup.html")
        if username == '' or email == '' or password1 == '' or password2 == '':
            return render(request,"Auth/signup.html")
        if password1 != password2 :
            return render(request,"Auth/signup.html")
        newuser = User.objects.create_user(username ,email, password1)
        newuser.first_name = fname
        newuser.last_name = lname
        newuser.save()
        messages.success(request,"Your account successfuly created.")
        return render(request,"Auth/login.html")
    return render(request,"Auth/signup.html")


def login_user(request):
    
    if request.method == "POST":
        username = request.POST['username1']
        password = request.POST['password1']
        if username=='admin' or password=='admin':
            return render(request,"Auth/login.html")
        user = authenticate(username=username, password=password)
        if user is not None:
            # A backend authenticated the credentials
            login(request,user)
            return render(request, 'user_profile.html')
        else:
           # No backend authenticated the credentials
            return render(request, 'Auth/login.html')
    return render(request, 'Auth/login.html')


      



def login_adminn(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        if username!='admin' or password!='admin':
            return render(request,"Auth/admin_login.html")
        user = authenticate(username=username, password=password)
        if user is not None:
            # A backend authenticated the credentials
            login(request,user)
            return render(request, 'admin_addapps.html')
        else:
           # No backend authenticated the credentials
            return render(request, 'Auth/admin_login.html')
    return render(request, 'Auth/admin_login.html')


def logoutUser(request):
    logout(request)
    return redirect("/login")

def user_home(request):
    results=app_details.objects.all()
    return render(request,"user_home.html",{'app_details':results})

def admin_home(request):
    results=app_details.objects.all()
    # print(results)
    return render(request,"admin_home.html",{"add_apps":results})

def user_profile(request):
    
    return render(request,"user_profile.html")

def user_point(request):
        return render(request,"user_points.html")


def add_apps(request):
    if request.method=="POST":
        if request.POST['app_name']=='' or request.POST['app_link']=='' or request.POST['app_catagory']=='' or request.POST['sub_catagory']=='' or request.POST['add_points']=='':
            return render(request,'admin_addapps.html')
        else:
            savest=app_details()
            print("2")
            savest.app_name=request.POST['app_name']
            savest.app_link=request.POST['app_link']
            savest.app_catagory=request.POST['app_catagory']
            savest.sub_catagory=request.POST['sub_catagory']
            savest.app_points=request.POST['add_points']
            savest.save()
            return render(request,"admin_addapps.html")
    return render(request,'admin_addapps.html')


def get_appdetail(request,app_name):
    getappdetails=app_details.objects.get(app_name=app_name)
    if request.method=="POST":
        print("1")

        if request.POST['appname']=='' or request.FILES['myfile']=='' or request.POST['apppoints']=='':
            return render(request,'user_task.html')
        else:
            save_task=user_submission()
            save_task.username=request.POST['username']
            save_task.appname=request.POST['appname']
            save_task.applink=request.POST['applink']
            save_task.points=request.POST['apppoints']
            save_task.screenshots=request.FILES['myfile']
            save_task.save()
            return render(request,"user_task.html")
    return render(request,'user_task.html',{"app_details":getappdetails})
    

def delete(request,app_name):
    getappdetails=app_details.objects.filter(app_name=app_name).first()
    getappdetails.delete()
    app_details.objects.all()
    return render(request,'admin_addapps.html',{"app_details":getappdetails})

def display(request):
    results=app_details.objects.all()
    return render(request,'user_home.html',{'app_details':results})


def task_submit(request):
    if request.method=="POST":
        print("1")

        if request.POST['app_name']=='' or request.FILES['myfile']=='' or request.POST['apppoints']=='':
            return render(request,'user_task.html')
        else:
            print("1")

            save_task=user_submission()
            save_task.username=request.POST['username']
            save_task.appname=request.POST['appname']
            save_task.applink=request.POST['applink']
            save_task.points=request.POST['apppoints']
            save_task.screenshots=request.FILES['myfile']
            save_task.save()
            return render(request,"user_task.html")
    return render(request,'user_task.html')



def getapi(request,id):
    if request.method=="GET":
        try:
            apiresult=app_details.objects.get(id=id)
            response = json.dumps([{'app_name':apiresult.app_name,'app_link':apiresult.app_link,'app_catagory':apiresult.app_catagory,'sub_catagory':apiresult.sub_catagory,'app_points':apiresult.app_points}])
        except:
            response = json.dumps([{'Error':'No detals found'}])
    return HttpResponse(response,content_type='text/json')
